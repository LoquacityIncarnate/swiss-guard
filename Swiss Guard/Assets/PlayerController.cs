using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 8;
    float smoothMoveMagnitude;
    float cameraAngle;
    public Camera currentCam;
    public float smoothMoveTime = .04f;
    float smoothMoveSpeed;
    public float turnSpeed = 16;
    float angle;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cameraAngle = currentCam.transform.eulerAngles.y;
        //transform.Rotate(0, 0, cameraAngle);
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(0,0,0);
        Vector3 inputVector = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        float moveActivation = inputVector.magnitude;
        smoothMoveMagnitude = Mathf.SmoothDamp(smoothMoveMagnitude, moveActivation, ref smoothMoveSpeed, smoothMoveTime);
        float targetAngle = Mathf.Atan2(inputVector.x, inputVector.z)*Mathf.Rad2Deg;
        angle = Mathf.LerpAngle(angle,targetAngle,turnSpeed* Time.deltaTime * moveActivation);
        
        transform.eulerAngles = (Vector3.up * angle);
        transform.Translate(Quaternion.Euler(0, cameraAngle, 0) * transform.forward * moveSpeed * Time.deltaTime*moveActivation, Space.World);
    }
}
