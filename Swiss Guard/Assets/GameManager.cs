using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Camera[] cameras;
    public int currentCam;
    // Start is called before the first frame update
    void Start()
    {
        cameras = FindObjectsOfType<Camera>();
        currentCam = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            currentCam--;
            if (currentCam < 0)
                currentCam = cameras.Length - 1;
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            currentCam++;
            if (currentCam >= cameras.Length)
                currentCam = 0;
        }
        for (int i = 0; i<cameras.Length; i++)
        {
            if (i == currentCam)
             cameras[i].enabled = true; 
            else
                cameras[i].enabled = false;
        }
    }
    
}
