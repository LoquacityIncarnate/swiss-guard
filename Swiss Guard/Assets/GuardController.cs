using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardController : MonoBehaviour
{
    public Transform pathLog;
    public float speed = 6;
    public float waitTime = 0.4f;
    public float turnSpeed = 135f;
    // Start is called before the first frame update
    void Start()
    {
        Vector3[] waypoints = new Vector3[pathLog.childCount];
        for (int i = 0; i < pathLog.childCount; i++)
        { waypoints[i] = pathLog.GetChild(i).transform.position;
            waypoints[i] = new Vector3(waypoints[i].x,transform.position.y,waypoints[i].z);
        }
        StartCoroutine(WalkPath(waypoints));
    }

    IEnumerator FaceTo(Vector3 target) {
        Vector3 direction = (target - transform.position).normalized;
        float targetAngle = 90 - Mathf.Atan2(direction.z, direction.x)*Mathf.Rad2Deg;
        while (Mathf.Abs(Mathf.DeltaAngle(transform.eulerAngles.y, targetAngle))>0.5)
        {
            float angle = Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetAngle, turnSpeed*Time.deltaTime);
            transform.eulerAngles = Vector3.up * angle;
            yield return null;
        }
    }
    IEnumerator WalkPath(Vector3[] waypoints) 
    {
        transform.position = waypoints[0];
        int next = 1;
        Vector3 nextPos = waypoints[next];
        transform.LookAt(nextPos);
        while (1 == 1)
        { transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
            if (transform.position == nextPos)
            { if (next + 1 == waypoints.Length) next = 0;else next++;
                nextPos = waypoints[next];
                yield return new WaitForSeconds(waitTime);
                yield return StartCoroutine(FaceTo(nextPos));
            }
            yield return null;
        }
        yield return null;
    }
    private void OnDrawGizmos()
    {
        Vector3 lastPos = pathLog.GetChild(0).transform.position;
        foreach (Transform point in pathLog)
        {
            Gizmos.DrawSphere(point.position,0.2f);
            Gizmos.DrawLine(point.position, lastPos);
            lastPos = point.position;
        }
        Gizmos.DrawLine(pathLog.GetChild(0).transform.position, lastPos);
    }
}
